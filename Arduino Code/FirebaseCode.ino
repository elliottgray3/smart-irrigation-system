#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif
#include <Espalexa.h>
#include <SoftwareSerial.h>
#include <FirebaseESP8266.h>
#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif

#define WIFI_SSID "Connectify-ESPBoard" // your wifi SSID
#define WIFI_PASSWORD "esp8266password" //your wifi PASSWORD

#define FIREBASE_HOST "greenie-dc274-default-rtdb.firebaseio.com" // change here
#define FIREBASE_AUTH "2K5AK56HoEIo7W90HqmoxnuC2h9XR2uwzG4V043q"  // your private key
FirebaseData firebaseData;
FirebaseData firebaseData2;

SoftwareSerial test(3, 1);

boolean stopValue = false;
boolean basilStopValue = false;
boolean thymeStopValue = false;
boolean bayLeafStopValue = false;
boolean lemongrassStopValue = false;
boolean oreganoStopValue = false;
boolean mintStopValue = false;
boolean sageStopValue = false;
boolean rosemaryStopValue = false;
boolean chivesStopValue = false;
boolean cilantroStopValue = false;
boolean automatedStopValue = false;

// prototypes
boolean connectWifi();

//callback functions
void firstLightChanged(uint8_t brightness);
void secondLightChanged(uint8_t brightness);
void thirdLightChanged(uint8_t brightness);
void fourthLightChanged(uint8_t brightness);
void fifthLightChanged(uint8_t brightness);

// device names
String Device_1_Name = "Water Pump";
String Device_2_Name = "Nutrient Pump";
String Device_3_Name = "Temperature Display";
String Device_4_Name = "Soil Display";
String Device_5_Name = "pH Display";
String Device_6_Name = "Rain Display";

boolean wifiConnected = false;

Espalexa espalexa;

void setup ()
{
  Serial.begin(9600);
  test.begin(9600);
  
  // Initialise wifi connection
  wifiConnected = connectWifi();

  if (wifiConnected)
  {
    // Define your devices here.
    espalexa.addDevice(Device_1_Name, firstLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_2_Name, secondLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_3_Name, thirdLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_4_Name, fourthLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_5_Name, fifthLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_6_Name, sixthLightChanged); //simplest definition, default state off

    espalexa.begin();
  }
  else
  {
    while (1)
    {
      Serial.println("Cannot connect to WiFi. Please check data and reset the ESP.");
      delay(2500);
    }
  }
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);     
}
void loop ()
{
  // Alexa Loop
  espalexa.loop();
  delay(1000);
  // Manual Water Pump Control
  if(Firebase.getString(firebaseData, "/ManualStatus"))
  {
    String manualstatus = firebaseData.stringData();
    if(manualstatus.toInt() == 1){
      if (stopValue == true)
      {
        test.write("1");
        stopValue = false;        
      }
    }
    else {
      if (stopValue == false)
      {
       test.write("0");
       stopValue = true; 
      }
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  } 
  // Automated Basil Control
  if(Firebase.getString(firebaseData, "/BasilStatus"))
  {
    String basilstatus = firebaseData.stringData();
    if(basilstatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(2000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (basilStopValue == false)
      {
       test.write("0");
       basilStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
  // Automated Thyme Control
  if(Firebase.getString(firebaseData, "/ThymeStatus"))
  {
    String thymestatus = firebaseData.stringData();
    if(thymestatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (thymeStopValue == false)
      {
       test.write("0");
       thymeStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
  // Automated Bay Leaf Control
  if(Firebase.getString(firebaseData, "/BayLeafStatus"))
  {
    String bayLeafstatus = firebaseData.stringData();
    if(bayLeafstatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (bayLeafStopValue == false)
      {
       test.write("0");
       bayLeafStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Lemongrass Control
  if(Firebase.getString(firebaseData, "/LemongrassStatus"))
  {
    String lemongrassStatus = firebaseData.stringData();
    if(lemongrassStatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (lemongrassStopValue == false)
      {
       test.write("0");
       lemongrassStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Oregano Control
  if(Firebase.getString(firebaseData, "/OreganoStatus"))
  {
    String oreganostatus = firebaseData.stringData();
    if(oreganostatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (oreganoStopValue == false)
      {
       test.write("0");
       oreganoStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Mint Control
  if(Firebase.getString(firebaseData, "/MintStatus"))
  {
    String mintstatus = firebaseData.stringData();
    if(mintstatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (mintStopValue == false)
      {
       test.write("0");
       mintStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Sage Control
  if(Firebase.getString(firebaseData, "/SageStatus"))
  {
    String sagestatus = firebaseData.stringData();
    if(sagestatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (sageStopValue == false)
      {
       test.write("0");
       sageStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Rosemary Control
  if(Firebase.getString(firebaseData, "/RosemaryStatus"))
  {
    String rosemarystatus = firebaseData.stringData();
    if(rosemarystatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (rosemaryStopValue == false)
      {
       test.write("0");
       rosemaryStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Chives Control
  if(Firebase.getString(firebaseData, "/ChivesStatus"))
  {
    String chivesStatus = firebaseData.stringData();
    if(chivesStatus.toInt() == 1){
     Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (chivesStopValue == false)
      {
       test.write("0");
       chivesStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }

  // Automated Cilantro Control
  if(Firebase.getString(firebaseData, "/CilantroStatus"))
  {
    String cilantrostatus = firebaseData.stringData();
    if(cilantrostatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() < 400){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        test.write("1");
        delay(4000);
        test.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        test.write("0");
      }
    }
    else {
      if (cilantroStopValue == false)
      {
       test.write("0");
       cilantroStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
}

//our callback functions
void firstLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On Water Pump
      test.write("1");
    }
  }
  else
  {
    // Turn Off Water Pump
    test.write("0");
  }
}

//our callback functions
void secondLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On Nutrient Pump
      test.write("3");
    }
  }
  else
  {
    // Turn Off Nutrient Pump
    test.write("2");
  }
}

//our callback functions
void thirdLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD Temperature/Humidity Display
      test.write("5");
    }
  }
  else
  {
    // Turn Off LCD Temperature/Humidity Display
    test.write("4");
  }
}

//our callback functions
void fourthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD Soil Display
      test.write("6");
    }
  }
  else
  {
    // Turn Off LCD Soil Display
    test.write("4");
  }
}


//our callback functions
void fifthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD pH Display
      test.write("7");
    }
  }
  else
  {
    // Turn Off LCD pH Display
    test.write("4");
  }
}

//our callback functions
void sixthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD Rain Display
      test.write("8");
    }
  }
  else
  {
    // Turn Off LCD Rain Display
    test.write("4");
  }
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi()
{
  boolean state = true;
  int i = 0;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 20) {
      state = false; break;
    }
    i++;
  }
  Serial.println("");
  if (state) {
    Serial.print("Connected to ");
    Serial.println(WIFI_SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("Connection failed.");
  }
  return state;
}
