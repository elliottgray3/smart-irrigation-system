#include <LiquidCrystal.h>
#include <DHT.h>

#define DHT_PIN 6
#define DHT_TYPE DHT11

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

DHT dht(DHT_PIN, DHT_TYPE);

void setup() {
  // Solenoid Valve Pin
  pinMode(A2, OUTPUT);
  // Nutrient Pump Pin
  pinMode(A1, OUTPUT);
  // Water Pump Pin
  pinMode(A0, OUTPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);
  dht.begin();
  while(!Serial);
  Serial.println("Input 1 to Turn LED on and 0 to off");
}

void loop() {
  
  lcd.setCursor(0, 0);
  lcd.print("Welcome to");
  lcd.setCursor(0, 1);
  lcd.print("Greenie!");
  delay(2000);
      
  if (Serial.available()){
    int state = Serial.parseInt();

    // LCD Rain Display
    if (state == 8){
      Serial.println("Rain Displayed");
    }
    // LCD pH Display
    if (state == 7){
      Serial.println("pH Displayed");
    }
    // LCD Soil Display
    if (state == 6){
      Serial.println("Soil Displayed");
    }
    // LCD Temp/Humidity Display
    if (state == 5){
      int chk = dht.read(DHT_PIN);
      float temp = dht.readTemperature();
      delay(10);
      float hum = dht.readHumidity();
      lcd.setCursor(0, 0);
      lcd.print("Temp: ");
      lcd.print(temp);
      lcd.print((char)223);
      lcd.print("C");
      lcd.setCursor(0, 1);
      lcd.print("Humidity: ");
      lcd.print(hum);
      lcd.print("%");
      delay(2000);
    }
    
    // LCD Temp/Humidity Data OFF
    if (state == 4){
      delay(10);
      lcd.setCursor(0, 0);
      lcd.print("Welcome to");
      lcd.setCursor(0, 1);
      lcd.print("Greenie!");
      delay(2000);
    }
    // Nutrient Pump Control
    if (state == 3){
      digitalWrite(A1, HIGH);
      Serial.println("Nutrient Pump ON");
    }
    if (state == 2){
      digitalWrite(A1, LOW);
      Serial.println("Nutrient Pump OFF"); 
    }
    // Water Pump Control
    if (state == 1){
      digitalWrite(A0, HIGH);
      Serial.println("Water Pump ON");
    }
    if (state == 0){
      digitalWrite(A0, LOW);
      Serial.println("Water Pump OFF");
    }
  }

}
