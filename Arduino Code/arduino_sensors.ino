
#include <DHT.h>
#include <SoftwareSerial.h>

#define DHT_TYPE DHT11
#define DHT_PIN A0
#define SOIL_PIN A1
#define RAIN_PIN A2
#define PH_PIN A3

int phVal = 0;
unsigned long int avgVal;
int buffer_arr[10], phTemp;
float phFinal;
float calibration_value = 21.34 - 0.8;

DHT dht(DHT_PIN, DHT_TYPE);
SoftwareSerial arduino_test(10, 11);

void setup() {
  pinMode(7, OUTPUT);
  Serial.begin(9600);
  arduino_test.begin(9600);
  //while(!Serial);
  dht.begin();
}

void loop() {
  
  if(arduino_test.available() > 0) {
    int state = arduino_test.parseInt();
   
    if (state == 1){
      digitalWrite(7, HIGH);
      Serial.println("LED ON");
    }
    if (state == 0){
      digitalWrite(7, LOW);
      Serial.println("LED OFF");
    }
  }
  
 
  if(Serial.available() == 0) {
    int chk = dht.read(DHT_PIN);
    float temp = dht.readTemperature()* 32/9;
    delay(100);
    float hum = dht.readHumidity();
    delay(100);
    int soil = analogRead(SOIL_PIN);
    delay(100);
    int rain = analogRead(RAIN_PIN);
    delay(100);
    ph_sensor();
    delay(100);
    arduino_test.print(temp);
    arduino_test.print("T");
    arduino_test.print(hum);
    arduino_test.print("H");
    arduino_test.print(soil);
    arduino_test.print("S");
    arduino_test.print(rain);
    arduino_test.print("R");
    arduino_test.print(phFinal);
    arduino_test.print("P");
    arduino_test.print("\n");
  

    /*
    Serial.print("Temp: ");
    Serial.print(temp);
    Serial.println("°F");
    Serial.print("Humidity: ");
    Serial.print(hum);
    Serial.println("%");
    Serial.print("Soil: ");
    Serial.println(soil);
    Serial.print("Rain: ");
    Serial.println(rain);
    Serial.print("pH Value: ");
    Serial.println(phFinal);
    */
  
    delay(2000);
  }
  
}

void ph_sensor() {
  for(int i=0;i<10;i++) { 
    buffer_arr[i]=analogRead(PH_PIN);
    delay(30);
  }
  
  for(int i=0;i<9;i++) {
    for(int j=i+1;j<10;j++) {
      if(buffer_arr[i]>buffer_arr[j]) {
        phTemp=buffer_arr[i];
        buffer_arr[i]=buffer_arr[j];
        buffer_arr[j]=phTemp;
      }
    }
  }
  
  avgVal=0;
  for(int i=2;i<8;i++) {
    avgVal+=buffer_arr[i];
  }
  float volt=(float)avgVal*5.0/1024/6; 
  phFinal = -5.70 * volt + calibration_value;
}
