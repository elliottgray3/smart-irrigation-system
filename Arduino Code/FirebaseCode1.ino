#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif
#include <Espalexa.h>
#include <SoftwareSerial.h>
#include <FirebaseESP8266.h>
#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif
#include <DHT.h>

#define SOIL_PIN D0
#define RAIN_PIN D4
#define DHT_PIN D2
#define DHT_TYPE DHT11

#define WIFI_SSID "YagLaser Nest" // your wifi SSID
#define WIFI_PASSWORD "Miles1126crv$" //your wifi PASSWORD

#define FIREBASE_HOST "greenie-dc274-default-rtdb.firebaseio.com" // change here
#define FIREBASE_AUTH "2K5AK56HoEIo7W90HqmoxnuC2h9XR2uwzG4V043q"  // your private key
FirebaseData firebaseData;

SoftwareSerial test(3, 1);

boolean stopValue = false;
boolean basilStopValue = false;
/* boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false; */

// prototypes
boolean connectWifi();

//callback functions
void firstLightChanged(uint8_t brightness);

// device names
String Device_1_Name = "Water Pump";

boolean wifiConnected = false;

Espalexa espalexa;

DHT dht(DHT_PIN, DHT_TYPE);

void setup ()
{
  Serial.begin(115200);
  test.begin(115200);
  
  // Initialise wifi connection
  wifiConnected = connectWifi();

  if (wifiConnected)
  {
    // Define your devices here.
    espalexa.addDevice(Device_1_Name, firstLightChanged); //simplest definition, default state off
    espalexa.begin();
  }
  else
  {
    while (1)
    {
      Serial.println("Cannot connect to WiFi. Please check data and reset the ESP.");
      delay(2500);
    }
  }
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);     
}
void loop ()
{
  // Alexa
  espalexa.loop();
  delay(1000);
  // Manual Water Pump Control
  if(Firebase.getString(firebaseData, "/ManualStatus"))
  {
    String manualstatus = firebaseData.stringData();
    if(manualstatus.toInt() == 1){
      if (stopValue == true)
      {
        test.write("1");
        stopValue = false;        
      }
      // digitalWrite(LedPin, LOW);
      // Serial.println("on");
    }
    else {
      // digitalWrite(LedPin, HIGH);
      if (stopValue == false)
      {
       test.write("0");
       // Serial.println("off");
       stopValue = true; 
      }
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  } 
  // Automated Basil Control
  if(Firebase.getString(firebaseData, "/BasilStatus"))
  {
    String basilstatus = firebaseData.stringData();
    if(basilstatus.toInt() == 1){
      test.write("3");
      delay(5000);
      test.write("2");
      basilStopValue = false; 
    }
    else {
      // digitalWrite(LedPin, HIGH);
      if (basilStopValue == false)
      {
       test.write("2");
       basilStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
  /*
  // Soil Moisture Sensor
  int soilReading = analogRead(SOIL_PIN);

  Serial.print("Soil Moisture Level: ");
  Serial.println(soilReading);

  Firebase.setFloat(firebaseData, "Sensors/SoilMoisture", soilReading);

  // Rain Sensor
  int rainReading = analogRead(RAIN_PIN);

  Serial.print("Rain Level: ");
  Serial.println(rainReading);

  Firebase.setFloat(firebaseData, "Sensors/Rain", rainReading);

  // pH Sensor
  */
  // Temperature and Humidity Sensor
  float tempReading = dht.readTemperature();
  float humReading = dht.readHumidity();
  
  Serial.print("Temperature: ");
  Serial.print(tempReading);
  Serial.println("°C");

  Serial.print("Humidity: ");
  Serial.print(humReading);
  Serial.println("%");

  Firebase.setFloat(firebaseData, "Sensors/Temperature", tempReading);
  Firebase.setFloat(firebaseData, "Sensors/Humidity", humReading);
  delay(3000);
  
}

//our callback functions
void firstLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On Water Pump
      test.write("3");
    }
  }
  else
  {
    // Turn Off Water Pump
    test.write("2");
  }
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi()
{
  boolean state = true;
  int i = 0;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 20) {
      state = false; break;
    }
    i++;
  }
  Serial.println("");
  if (state) {
    Serial.print("Connected to ");
    Serial.println(WIFI_SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("Connection failed.");
  }
  return state;
}
