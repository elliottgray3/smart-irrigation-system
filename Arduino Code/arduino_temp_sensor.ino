
#include <DHT.h>
#include <SoftwareSerial.h>

SoftwareSerial arduino_test(1, 0);

#define DHT_PIN A0
#define DHT_TYPE DHT11

DHT dht(DHT_PIN, DHT_TYPE);

void setup() {
  Serial.begin(115200);
  arduino_test.begin(9600);
  dht.begin();
}

void loop() {
  int chk = dht.read(DHT_PIN);
  float temp = dht.readTemperature()* 32/9;
  delay(10);
  float hum = dht.readHumidity();
  //arduino_test.print("Hello\n");
  delay(10);
  //arduino_test.write(temp);
  delay(10);
  //arduino_test.write(hum);

  //String reading = String(temp) + String(" ") + String(hum);
  //Serial.print("Temp: ");
  Serial.println(temp);
  //Serial.println("°F");
  //Serial.print("Humidity: ");
  Serial.println(hum);
  //Serial.println("%");
  //Serial.println(reading);
  
  delay(2000);
  
}
