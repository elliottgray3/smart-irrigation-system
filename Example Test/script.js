// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyCtgqDlOObAqXaR_p1iYbwgjVtdaAc_UmA",
    authDomain: "greenie-dc274.firebaseapp.com",
    databaseURL: "https://greenie-dc274-default-rtdb.firebaseio.com",
    projectId: "greenie-dc274",
    storageBucket: "greenie-dc274.appspot.com",
    messagingSenderId: "409338776023",
    appId: "1:409338776023:web:f918e7e46b56924a9da392",
    measurementId: "G-7W9YY3DQDX"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

$(document).ready(function(){
    var database = firebase.database();
	var Led1Status;

	database.ref().on("value", function(snap){
		Led1Status = snap.val().Led1Status;
		if(Led1Status == "1"){    // check from the firebase
			//$(".Light1Status").text("The light is off");
			document.getElementById("unact").style.display = "none";
			document.getElementById("act").style.display = "block";
		} else {
			//$(".Light1Status").text("The light is on");
			document.getElementById("unact").style.display = "block";
			document.getElementById("act").style.display = "none";
		}
	});

    $(".toggle-btn").click(function(){
		var firebaseRef = firebase.database().ref().child("Led1Status");

		if(Led1Status == "1"){    // post to firebase
			firebaseRef.set("0");
			Led1Status = "0";
		} else {
			firebaseRef.set("1");
			Led1Status = "1";
		}
	})
});