import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import Herbs from './pages/Herbs';
import History from './pages/History';
import About from './pages/About';

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/herbs' component={Herbs} />
          <Route path='/history' component={History} />
          <Route path='/about' component={About} />
        </Switch>
      </Router>
    </>
  );
}

export default App;