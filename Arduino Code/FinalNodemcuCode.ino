#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif
#include <Espalexa.h>
#include <SoftwareSerial.h>
#include <FirebaseESP8266.h>
#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif

#define WIFI_SSID "Connectify-ESPBoard" // your wifi SSID
#define WIFI_PASSWORD "esp8266password" //your wifi PASSWORD

#define FIREBASE_HOST "greenie-dc274-default-rtdb.firebaseio.com" // change here
#define FIREBASE_AUTH "2K5AK56HoEIo7W90HqmoxnuC2h9XR2uwzG4V043q"  // your private key
FirebaseData firebaseData;
FirebaseData firebaseData2;
FirebaseData firebaseData3;

SoftwareSerial nodemcu(D1, D2);
SoftwareSerial nodemcu2(3, 1);

boolean stopValue = false;
boolean mintStopValue = false;
boolean automatedStopValue = false;
boolean sensorStopValue = true;

// prototypes
boolean connectWifi();

//callback functions
void firstLightChanged(uint8_t brightness);
void secondLightChanged(uint8_t brightness);
void thirdLightChanged(uint8_t brightness);
void fourthLightChanged(uint8_t brightness);
void fifthLightChanged(uint8_t brightness);
void sixthLightChanged(uint8_t brightness);
void parse_data();

// device names
String Device_1_Name = "Water Pump";
String Device_2_Name = "Nutrient Pump";
String Device_3_Name = "Temperature Display";
String Device_4_Name = "Soil Display";
String Device_5_Name = "pH Display";
String Device_6_Name = "Rain Display";

boolean wifiConnected = false;

Espalexa espalexa;

// Variables
char c;
String dataIn;
int indexOfTemp, indexOfHum, indexOfSoil, indexOfRain, indexOfPh;
String tempReading, humReading, soilReading, rainReading, phReading;

void setup ()
{
  Serial.begin(9600);
  nodemcu.begin(9600);
  
  // Initialise wifi connection
  wifiConnected = connectWifi();

  if (wifiConnected)
  {
    // Define your devices here.
    espalexa.addDevice(Device_1_Name, firstLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_2_Name, secondLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_3_Name, thirdLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_4_Name, fourthLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_5_Name, fifthLightChanged); //simplest definition, default state off
    espalexa.addDevice(Device_6_Name, sixthLightChanged); //simplest definition, default state off

    espalexa.begin();
  }
  else
  {
    while (1)
    {
      Serial.println("Cannot connect to WiFi. Please check data and reset the ESP.");
      delay(2500);
    }
  }

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);     
}

void loop ()
{ 
  // Alexa Loop
  espalexa.loop();
  delay(1000);
  // Manual Water Pump Control

    if(Firebase.getString(firebaseData, "/ManualStatus"))
  {
      String manualstatus = firebaseData.stringData();
     
      if(manualstatus.toInt() == 1){
        sensorStopValue = false;      
        if (sensorStopValue == false){
          if (stopValue == true)
         {
           nodemcu2.write("1");
           stopValue = false;        
          }
         }       
      }
      else {
        sensorStopValue = true;
        if (stopValue == false)
        {
         nodemcu2.write("0");
         stopValue = true; 
        }
      }
   
  }
  else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
  // Automated Mint Control
  if(Firebase.getString(firebaseData, "/MintStatus"))
  {
    String mintstatus = firebaseData.stringData();
    if(mintstatus.toInt() == 1){
      Firebase.getString(firebaseData2, "/Sensors/Soil");
      String SoilStatus = firebaseData2.stringData();
      if (SoilStatus.toInt() > 600){
        automatedStopValue = true;
      }
      else{
        automatedStopValue = false;
      }
      if (automatedStopValue == true){
        nodemcu2.write("1");
        delay(2000);
        Firebase.getString(firebaseData3, "/WaterCount/TotalWater");
        String totalWaterString = firebaseData3.stringData();
        float totalWater = totalWaterString.toFloat();
        totalWater += 76.92;
        Firebase.setFloat(firebaseData, "/WaterCount/TotalWater", totalWater);
        nodemcu2.write("0");
        delay(4000);
      }
      else{
        automatedStopValue = false;
        nodemcu2.write("0");
      }
    }
    else {
      if (mintStopValue == false)
      {
       nodemcu2.write("0");
       mintStopValue = true; 
      } 
    }
  }else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
   
  if (sensorStopValue == true){
      while (nodemcu.available()>0) {
        c = nodemcu.read();
        if (c == '\n') 
        break;
        
        else 
        dataIn += c;
      }
     if (c == '\n') {
      parse_data(); 
      c = 0;
      dataIn = "";
    }
  }
  
  Firebase.setString(firebaseData, "Sensors/Temperature", tempReading);
  Firebase.setString(firebaseData, "Sensors/Humidity", humReading);
  Firebase.setString(firebaseData, "Sensors/Soil", soilReading);
  Firebase.setString(firebaseData, "Sensors/Rain", rainReading);
  Firebase.setString(firebaseData, "Sensors/pH", phReading);
  
}

//our callback functions
void firstLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On Water Pump
      nodemcu2.write("1");
    }
  }
  else
  {
    // Turn Off Water Pump
    nodemcu2.write("0");
  }
}

//our callback functions
void secondLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On Nutrient Pump
      nodemcu2.write("3");
    }
  }
  else
  {
    // Turn Off Nutrient Pump
    nodemcu2.write("2");
  }
}

//our callback functions
void thirdLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD Temperature/Humidity Display
      nodemcu.write("5");
    }
  }
  else
  {
    // Turn Off LCD Temperature/Humidity Display
    nodemcu.write("4");
  }
}

//our callback functions
void fourthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD Soil Display
      nodemcu.write("6");
    }
  }
  else
  {
    // Turn Off LCD Soil Display
    nodemcu.write("4");
  }
}


//our callback functions
void fifthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD pH Display
      nodemcu.write("7");
    }
  }
  else
  {
    // Turn Off LCD pH Display
    nodemcu.write("4");
  }
}

//our callback functions
void sixthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On LCD Rain Display
      nodemcu.write("8");
    }
  }
  else
  {
    // Turn Off LCD Rain Display
    nodemcu.write("4");
  }
}

void parse_data() {
  indexOfTemp = dataIn.indexOf("T");
  indexOfHum = dataIn.indexOf("H");
  indexOfSoil = dataIn.indexOf("S");
  indexOfRain = dataIn.indexOf("R");
  indexOfPh = dataIn.indexOf("P");

  tempReading = dataIn.substring(0, indexOfTemp);
  humReading = dataIn.substring(indexOfTemp+1, indexOfHum);
  soilReading = dataIn.substring(indexOfHum+1, indexOfSoil);
  rainReading = dataIn.substring(indexOfSoil+1, indexOfRain);
  phReading = dataIn.substring(indexOfRain+1, indexOfPh);
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi() {
  boolean state = true;
  int i = 0;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 20) {
      state = false; 
      break;
    }
    i++;
  }
  Serial.println("");
  if (state) {
    Serial.print("Connected to ");
    Serial.println(WIFI_SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("Connection failed.");
  }
  return state;
}
