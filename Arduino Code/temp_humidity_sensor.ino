 /* Measures the current temperature and humidity
 *  ans displays it on the LCD screen
 */

#include <LiquidCrystal.h>
#include <DHT.h>

#define DHT_PIN 7
#define DHT_TYPE DHT11

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

DHT dht(DHT_PIN, DHT_TYPE);

void setup() {
  lcd.begin(16, 2);
  dht.begin();
}

void loop() {
  int chk = dht.read(DHT_PIN);
  float temp = dht.readTemperature();
  delay(10);
  float hum = dht.readHumidity();
  lcd.setCursor(0, 0);
  lcd.print("Temp: ");
  lcd.print(temp);
  lcd.print((char)223);
  lcd.print("C");
  lcd.setCursor(0, 1);
  lcd.print("Humidity: ");
  lcd.print(hum);
  lcd.print("%");
  delay(2000);
  lcd.clear();
}
