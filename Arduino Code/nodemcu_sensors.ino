#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif
#include <Espalexa.h>
#include <SoftwareSerial.h>
#include <FirebaseESP8266.h>
#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif

#define WIFI_SSID "Connectify-ESPBoard" // your wifi SSID
#define WIFI_PASSWORD "esp8266password" //your wifi PASSWORD

#define FIREBASE_HOST "greenie-dc274-default-rtdb.firebaseio.com" // change here
#define FIREBASE_AUTH "2K5AK56HoEIo7W90HqmoxnuC2h9XR2uwzG4V043q"  // your private key
FirebaseData firebaseData;

SoftwareSerial nodemcu(D1, D2);
//SoftwareSerial test(3, 1);

boolean stopValue = false;
//boolean basilStopValue = false;
/* boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false;
boolean basilStopValue = false; */

// prototypes
boolean connectWifi();

//callback functions
void firstLightChanged(uint8_t brightness);
void parse_data();

// device names
String Device_1_Name = "Water Pump";

boolean wifiConnected = false;

Espalexa espalexa;

// Variables
char c;
String dataIn;
int indexOfTemp, indexOfHum, indexOfSoil, indexOfRain, indexOfPh;
String tempReading, humReading, soilReading, rainReading, phReading;


void setup ()
{
  Serial.begin(9600);
  //test.begin(9600);
  nodemcu.begin(9600);
  
  // Initialise wifi connection
  wifiConnected = connectWifi();
  /*
  if (wifiConnected)
  {
    // Define your devices here.
    espalexa.addDevice(Device_1_Name, firstLightChanged); //simplest definition, default state off
    espalexa.begin();
  }
  else
  {
    while (1)
    {
      Serial.println("Cannot connect to WiFi. Please check data and reset the ESP.");
      delay(2500);
    }
  }
  */
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);     
}

void loop ()
{
  /*
  // Alexa
  espalexa.loop();
  delay(1000);
  // Manual Water Pump Control
  
  if(Firebase.getString(firebaseData, "/ManualStatus"))
  {
    String manualstatus = firebaseData.stringData();
    
    if(manualstatus.toInt() == 1){
      sensorStopValue = false;
    
      if (stopValue == true)
      {
        test.write("1");
        stopValue = false;        
      }
      // digitalWrite(LedPin, LOW);
      // Serial.println("on");
    }
    else {
      sensorStopValue = true;
      // digitalWrite(LedPin, HIGH);
      if (stopValue == false)
      {
       test.write("0");
       // Serial.println("off");
       stopValue = true; 
      }
    }
  }
  else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
  
  // Automated Basil Control
  if(Firebase.getString(firebaseData, "/BasilStatus"))
  {
    String basilstatus = firebaseData.stringData();
    if(basilstatus.toInt() == 1){
      nodemcu_test.write("3");
      delay(5000);
      test.write("2");
      basilStopValue = false; 
    }
    else {
      // digitalprint(LedPin, HIGH);
      if (basilStopValue == false)
      {
       test.write("2");
       basilStopValue = true; 
      } 
    }
  }
  else{
    Serial.print("Error in getInt, ");
    Serial.println(firebaseData.errorReason());
  }
 */
  
  if (Serial.available() == 0) {
    nodemcu.print("1");
    delay(5000);
    nodemcu.print("0");
    delay(5000);
  }
  
  
  while (nodemcu.available()>0) {
    c = nodemcu.read();
    
    if (c == '\n') 
      break;
    else 
      dataIn += c;
  }
 
  if (c == '\n') {
    parse_data();
    /*
    Serial.print("Temperature: ");
    Serial.print(tempReading);
    Serial.println("°F");
    Serial.print("Humidity: ");
    Serial.print(humReading);
    Serial.println("%");
    Serial.print("Soil: ");
    Serial.println(soilReading);
    */
    
    c = 0;
    dataIn = "";
  }
  
  Firebase.setString(firebaseData, "Sensors/Temperature", tempReading);
  Firebase.setString(firebaseData, "Sensors/Humidity", humReading);
  Firebase.setString(firebaseData, "Sensors/Soil", soilReading);
  Firebase.setString(firebaseData, "Sensors/Rain", rainReading);
  Firebase.setString(firebaseData, "Sensors/pH", phReading);
  
}

void parse_data() {
  indexOfTemp = dataIn.indexOf("T");
  indexOfHum = dataIn.indexOf("H");
  indexOfSoil = dataIn.indexOf("S");
  indexOfRain = dataIn.indexOf("R");
  indexOfPh = dataIn.indexOf("P");

  tempReading = dataIn.substring(0, indexOfTemp);
  humReading = dataIn.substring(indexOfTemp+1, indexOfHum);
  soilReading = dataIn.substring(indexOfHum+1, indexOfSoil);
  rainReading = dataIn.substring(indexOfSoil+1, indexOfRain);
  phReading = dataIn.substring(indexOfRain+1, indexOfPh);
}

/*
//our callback functions
void firstLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness)
  {
    if (brightness == 255)
    {
      // Turn On Water Pump
      test.print("3");
    }
  }
  else
  {
    // Turn Off Water Pump
    test.print("2");
  }
}
*/

// connect to wifi – returns true if successful or false if not
boolean connectWifi() {
  boolean state = true;
  int i = 0;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 20) {
      state = false; 
      break;
    }
    i++;
  }
  Serial.println("");
  if (state) {
    Serial.print("Connected to ");
    Serial.println(WIFI_SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("Connection failed.");
  }
  return state;
}
